<?php
/**
 * Created by PhpStorm.
 * User: TYVVTG
 * Date: 23-05-20
 * Time: 14:01
 */

namespace AppBundle\Form;
use AppBundle\Entity\Soortactiviteit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class SoortActiviteitenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('naam',TextType::class)
            ->add('min_leeftijd',TextType::class)
         ->add('tijdsduur', TextType::class)
        ->add('prijs', TextType::class);

    }
}