<?php
/**
 * Created by PhpStorm.
 * User: TYVVTG
 * Date: 23-05-20
 * Time: 20:52
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;

class WachtwoordWijzigenForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('plainPassword', RepeatedType::class, array(
        'type' => PasswordType::class,
        'first_options'  => array('label' => 'Nieuw Wachtwoord'),
        'second_options' => array('label' => 'Herhaal Nieuw Wachtwoord')));
    }
}